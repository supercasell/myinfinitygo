
FROM golang:alpine AS builder

WORKDIR $GOPATH/src/gitlab.com/supercasell/myinfinitygo

COPY . .
RUN apk add musl 
RUN CGO_ENABLED=0 go build -ldflags '-extldflags "-static"' -o /go/bin/myinfinitycli cmd/myinfinitycli.go

############################

FROM alpine

COPY --from=builder /go/bin/myinfinitycli /go/bin/myinfinitycli
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
USER 1001
ENTRYPOINT ["/go/bin/myinfinitycli"]
