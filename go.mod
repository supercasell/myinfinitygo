module gitlab.com/supercasell/myinfinitygo

go 1.16

require (
	github.com/coduno/netrc v0.0.0-20150824070116-8560df9760ef
	github.com/howeyc/gopass v0.0.0-20170109162249-bf9dde6d0d2c
	golang.org/x/crypto v0.0.0-20190313024323-a1f597ede03a // indirect
	golang.org/x/sys v0.0.0-20190312061237-fead79001313 // indirect
)
