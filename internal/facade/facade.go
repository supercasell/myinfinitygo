package facade

import (
	"bufio"
	"fmt"
	"io"
	"net/http"
	"net/http/cookiejar"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/supercasell/myinfinitygo/internal/api"
)

var retime = regexp.MustCompile(`^.* ([0-9]{2})-([0-9]{4}) .*$`)
var cu_regex = regexp.MustCompile(`Certificazione Unica redditi ([0-9]{4}) .*`)
var cmd_regex = regexp.MustCompile(`"([0-9a-f]{32})"`)

func New(vendor, user, pass string) (*MyInfinityFacade, error) {
	jar, _ := cookiejar.New(nil)

	myfa := &MyInfinityFacade{
		Vendor:    vendor,
		User:      user,
		Passw:     pass,
		apiClient: &http.Client{Jar: jar},
	}

	myfa.login()
	return myfa, nil
}

type MyInfinityFacade struct {
	Vendor    string
	User      string
	Passw     string
	apiClient *http.Client
}

type Payroll struct {
	Id         string
	OutputName string
	PubDate    string
	Refperiod  []string
}

func (myf MyInfinityFacade) GetPayRollList(count int) <-chan Payroll {
	responsePdfList := api.GetPDFList(myf.apiClient, myf.Vendor, count, myf.getCMDHash(myf.Vendor, api.API_CMD_STRING))
	channel := make(chan Payroll)
	go func() {
		defer close(channel)
		for rawpayroll := range responsePdfList.GetListOfMapFromResponse() {
			monthyear := retime.FindStringSubmatch(rawpayroll["dsfolder"])
			if len(monthyear) > 0 {
				channel <- Payroll{
					Id:         rawpayroll["idfolder"],
					OutputName: fmt.Sprintf("%s-%s_payroll_%s.pdf", monthyear[2], monthyear[1], rawpayroll["idfolder"]),
					PubDate:    rawpayroll["dtpubb"],
					Refperiod:  []string{monthyear[2], monthyear[1]},
				}
			} else {
				cud := cu_regex.FindStringSubmatch(rawpayroll["dsfolder"])
				channel <- Payroll{
					Id:         rawpayroll["idfolder"],
					OutputName: fmt.Sprintf("%s-00_CUD_%s.pdf", cud[1], rawpayroll["idfolder"]),
					PubDate:    rawpayroll["dtpubb"],
					Refperiod:  []string{cud[1], "00"},
				}
			}
		}
	}()
	return channel
}

func (myf MyInfinityFacade) DownloadLastPayroll(counter int, outputFolder string) {
	for payroll := range myf.GetPayRollList(counter) {
		myf.writeToFile(payroll, outputFolder)
	}
}

func (myf MyInfinityFacade) DownloadSpecificPayroll(outputFolder string, payrollDate string) {
	for payroll := range myf.GetPayRollList(10) {
		if fmt.Sprintf("%s-%s", payroll.Refperiod[0], payroll.Refperiod[1]) == payrollDate {
			myf.writeToFile(payroll, outputFolder)
		}
	}
}

func (myf MyInfinityFacade) writeToFile(pdf Payroll, outputFolder string) {
	outputFile := filepath.Join(outputFolder, pdf.OutputName)
	if _, err := os.Stat(outputFile); os.IsNotExist(err) {
		out, err := os.Create(outputFile)
		if err != nil {
			panic(err)
		}
		defer out.Close()
		repsBody := api.GetBodyPDF(myf.apiClient, myf.Vendor, pdf.Id)
		defer repsBody.Close()
		_, _ = io.Copy(out, repsBody)
	}
}

func (myf MyInfinityFacade) login() {
	api.Login(myf.apiClient, myf.Vendor, myf.User, myf.Passw)
}

func (myf MyInfinityFacade) getCMDHash(vendor, cmd string) string {
	responseGetCMD := api.GetCMDHash(myf.apiClient, vendor, cmd)
	defer responseGetCMD.Close()

	scanner := bufio.NewScanner(responseGetCMD)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, cmd) {
			cmdHash := cmd_regex.FindStringSubmatch(line)
			return cmdHash[1]
		}
	}

	panic("Unabled to obtain cmd hash")

}

func (myf MyInfinityFacade) Close() {
	api.Logout(myf.apiClient, myf.Vendor)
}
