package api

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

const HOSTNAME = "https://www.myinfinityportal.it"
const BASEPATH = "%s/servlet"
const JSP_BASEPATH = "%s/jsp"
const LOGIN_ENDPOINT = "cp_login"
const LIST_PDF_ENDPOINT = "SQLDataProviderServer"
const DOWNLOAD_PDF_ENDPOINT = "ushp_bexecdoc"
const LOGOUT_ENDPOINT = "usut_blogout"

const GET_CMDHASH_ENDPOINT = "ushp_one_column_model.jsp"

var LOGIN_URL = fmt.Sprintf("%s/%s/%s", HOSTNAME, BASEPATH, LOGIN_ENDPOINT)
var LIST_PDF_URL = fmt.Sprintf("%s/%s/%s", HOSTNAME, BASEPATH, LIST_PDF_ENDPOINT)
var DOWNLOAD_PDF_URL = fmt.Sprintf("%s/%s/%s", HOSTNAME, BASEPATH, DOWNLOAD_PDF_ENDPOINT)
var LOGOUT_URL = fmt.Sprintf("%s/%s/%s", HOSTNAME, BASEPATH, LOGOUT_ENDPOINT)

var GET_CMDHASH_URL = fmt.Sprintf("%s/%s/%s", HOSTNAME, JSP_BASEPATH, GET_CMDHASH_ENDPOINT)
var API_CMD_STRING = "ushp_qfolderemsel_noread"

func Login(client *http.Client, vendor, username, password string) {
	loginURL := fmt.Sprintf(LOGIN_URL, vendor)
	inputValues := url.Values{"m_cUserName": {username}, "m_cPassword": {password}, "m_cAction": {"login"}}
	resp, err := client.PostForm(loginURL, inputValues)
	if err != nil {
		defer resp.Body.Close()
	}
}

func Logout(client *http.Client, vendor string) {
	logoutURL := fmt.Sprintf(LOGOUT_URL, vendor)
	resp, err := client.Get(logoutURL)
	if err == nil {
		defer resp.Body.Close()
	}
}

type ResponsePdfList struct {
	Data   [][]string
	Fields []string
}

func (pdf ResponsePdfList) GetListOfMapFromResponse() <-chan map[string]string {
	channel := make(chan map[string]string)
	go func() {
		defer close(channel)
		for _, dataItemList := range pdf.Data {
			payroollDetails := make(map[string]string)
			for counter, dataItem := range dataItemList {
				payroollDetails[pdf.Fields[counter]] = dataItem
			}
			if len(payroollDetails) != 0 {
				channel <- payroollDetails
			}
		}
	}()
	return channel
}

func GetPDFList(client *http.Client, vendor string, rows int, cmdhash string) ResponsePdfList {
	listPDFUrl := fmt.Sprintf(LIST_PDF_URL, vendor)
	inputValues := url.Values{
		"rows":      {fmt.Sprintf("%d", rows)},
		"startrow":  {"0"},
		"count":     {"false"},
		"sqlcmd":    {API_CMD_STRING},
		"orderby":   {"chkread, dtstartview desc, LOGICFOLDERPATH"},
		"pFLVIEWNG": {"S"},
		"cmdhash":   {cmdhash},
	}
	resp, err := client.PostForm(listPDFUrl, inputValues)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	var result ResponsePdfList
	_ = json.NewDecoder(resp.Body).Decode(&result)
	return result
}

func GetBodyPDF(client *http.Client, vendor, idPDF string) io.ReadCloser {
	downloadPDFURL := fmt.Sprintf(DOWNLOAD_PDF_URL, vendor)
	inputValues := url.Values{"idfolder": {idPDF}}
	resp, err := client.PostForm(downloadPDFURL, inputValues)
	if err != nil {
		panic(err)
	}
	return resp.Body
}

func GetCMDHash(client *http.Client, vendor, cmd string) io.ReadCloser {
	getCMDHashUrl := fmt.Sprintf(GET_CMDHASH_URL, vendor)
	resp, err := client.Get(getCMDHashUrl)
	if err != nil {
		panic(err)
	}
	return resp.Body
}
