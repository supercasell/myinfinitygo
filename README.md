# ![Logo](/img/the-infinity-gauntlet64.png) myinfinity GoCLI

myinfinitycli is a Go CLI application 

## Basic setup

Run the application:
```
$ myinfinitycli --help
```

The application will prompt for credentials if not provided by command line options or .netrc integration.

To use .netrc integration, create an entry in ~/.netrc file as follows:
```
machine https://www.myinfinityportal.it
    login USERNAME
    account VENDOR
    password PASSWORD
```

To override .netrc account credentials pass command line options:
```
$ myinfinitycli list --vendor VENDOR --user $USER --passw $PASS
```

Get the list of last the 10 payroll:
```
$ myinfinitycli list
```

Get the list of last the N payroll:
```
$ myinfinitycli list --count N
```

Download the latest payroll:
```
$ myinfinitycli download-last --output-folder ~/Downloads/
```

Download the latest N payroll:
```
$ myinfinitycli download-last --count N --output-folder ~/Downloads/
```

Download the specific payroll:
```
$ myinfinitycli download --payroll-date 2018-10 --output-folder ~/Downloads/
```


<div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
