package main

import (
	"flag"
	"os"
	"fmt"		
	"gitlab.com/supercasell/myinfinitygo/internal/facade"
	"github.com/coduno/netrc"
	"github.com/howeyc/gopass"
)

func main() {		
	
	downloadLastCommand := flag.NewFlagSet("download-last", flag.ExitOnError)
	downloadCommand := flag.NewFlagSet("download", flag.ExitOnError)
    listCommand := flag.NewFlagSet("list", flag.ExitOnError)

	listVendor := listCommand.String("vendor", "", "Myinfinityportal vendor. (Required)")
	listUser := listCommand.String("user", "", "Myinfinityportal user. (Required)")
	listPassw := listCommand.String("passw", "", "Myinfinityportal password. (Required)")
	listCount := listCommand.Int("count", 10, "Number of rows, default 10")

	downloadVendor := downloadCommand.String("vendor", "", "Myinfinityportal vendor. (Required)")
	downloadUser  := downloadCommand.String("user", "", "Myinfinityportal user. (Required)")
	downloadPassw := downloadCommand.String("passw", "", "Myinfinityportal password. (Required)")
	downloadPayrolldate := downloadCommand.String("payroll-date", "", "Payroll date to download in YYYY-MM format (e.g. 2018-10) (Required)")
	downloadOutPutFolder := downloadCommand.String("output-folder", "", "Download destination folder (Required)")

	downloadLastVendor := downloadLastCommand.String("vendor", "", "Myinfinityportal vendor. (Required)")
	downloadLastUser  := downloadLastCommand.String("user", "", "Myinfinityportal user. (Required)")
	downloadLastPassw := downloadLastCommand.String("passw", "", "Myinfinityportal password. (Required)")
	downloadLastCount := downloadLastCommand.Int("count", 1, "Number of rows, default 10")
	downloadLastOutPutFolder := downloadLastCommand.String("output-folder", "", "Download destination folder (Required)")

    if len(os.Args) < 2 {
        fmt.Println("list, download, download-last subcommand is required")
        os.Exit(1)
	}
	
    switch os.Args[1] {
    case "list":
        _ = listCommand.Parse(os.Args[2:])
    case "download":
		_ = downloadCommand.Parse(os.Args[2:])
	case "download-last":
        _ = downloadLastCommand.Parse(os.Args[2:])
    default:
        flag.PrintDefaults()
        os.Exit(1)
    }

    if listCommand.Parsed() {		
		vendor, user, pass := getCredentials(listVendor, listUser, listPassw)
		getPayrollList(vendor, user, pass, *listCount)
	}

	if downloadCommand.Parsed() {
        if *downloadOutPutFolder == "" || *downloadPayrolldate == "" {
			downloadCommand.PrintDefaults()
			os.Exit(1)
		}
		vendor, user, pass := getCredentials(downloadVendor, downloadUser, downloadPassw)
		downloadPayroll(vendor, user, pass, *downloadOutPutFolder, *downloadPayrolldate)
	}
	
	if downloadLastCommand.Parsed() {
        if *downloadLastOutPutFolder == "" {
			downloadLastCommand.PrintDefaults()
			os.Exit(1)
		}
		vendor, user, pass := getCredentials(downloadLastVendor, downloadLastUser, downloadLastPassw)
		downloadLastPayroll(vendor, user, pass, *downloadLastCount, *downloadLastOutPutFolder)
	}
	
}

func getPayrollList(vendor,	user, passw string,	count int){
	myf, _ := facade.New(vendor, user, passw)		
	fmt.Println("|__ Pubblication date___|_______________file_name______________")
    for pdf := range myf.GetPayRollList(count){		
		fmt.Printf("| %s | %s \n",pdf.PubDate, pdf.OutputName)
	}            

	defer myf.Close()
}

func downloadPayroll(vendor, user, passw, outputDir, payrolldate string){
	myf, _ := facade.New(vendor, user, passw)	
	myf.DownloadSpecificPayroll(outputDir, payrolldate)
	defer myf.Close()
}

func downloadLastPayroll(vendor, user, passw string, count int, outputDir string){
	myf, _ := facade.New(vendor, user, passw)	
	myf.DownloadLastPayroll(count, outputDir)
	defer myf.Close()
}

func getCredentials(vendor, user, pass *string) (string, string, string){
	if *vendor != "" && *user != "" && *pass != "" {
		return *vendor, *user, *pass
	} else {
		entries, _ := netrc.Parse()		
		if entry, ok := entries["https://www.myinfinityportal.it"]; ok {						
			return entry.Account, entry.Login, entry.Password			
		}
		return getPromptCredentials()
	}	
}

func getPromptCredentials() (string, string, string){

	fmt.Print("Prease ender a vendor: ")
    var vendor string
    fmt.Scanln(&vendor)
	
	fmt.Print("Please enter a username: ")
    var user string
    fmt.Scanln(&user)
	
	fmt.Printf("Please enter a password: ")
	pass, err := gopass.GetPasswd()
	passS := string(pass[:])

	if err != nil {
		os.Exit(1)
	}
	return vendor, user, passS
}
