BINARY := myinfinitycli
VERSION ?= latest
PKGS := $(shell go list ./... | grep -v /vendor)

.PHONY: test
test: lint
	go test $(PKGS)

BIN_DIR := $(shell go env GOPATH)/bin
GOLANGCILINTER := $(BIN_DIR)/golangci-lint

$(GOLANGCILINTER):
	$(shell curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b ${BIN_DIR} v1.15.0)

.PHONY: lint
lint: $(GOLANGCILINTER)
	golangci-lint run ./... 

.PHONY: linux
linux:
	mkdir -p release
	GOOS=linux GOARCH=amd64 go build -o release/$(BINARY)-$(VERSION)-linux-amd64 cmd/myinfinitycli.go

.PHONY: linux-386
linux-386:
	mkdir -p release
	GOOS=linux GOARCH=386 go build -o release/$(BINARY)-$(VERSION)-linux-386 cmd/myinfinitycli.go	

.PHONY: raspb
raspb:
	mkdir -p release
	GOOS=linux GOARCH=arm GOARM=5 go build -o release/$(BINARY)-$(VERSION)-linux-arm5 cmd/myinfinitycli.go	


.PHONY: release
release: linux linux-386 raspb

.PHONY: clean
clean:
	rm -r release